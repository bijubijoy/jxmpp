/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package xmpp;

import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import sun.misc.BASE64Encoder;



/**
 *
 * @author BIJOY
 */
public class XMPP implements Runnable
{
    /**
     * @param args the command line arguments
     */
    // Variables
    public static String XMPPUser;
    private String XMPPPass;
    private String XMPPSer;
    public static String XMPPDom;
    private XMLOutputFactory factory;
    public static XMLStreamWriter writer;
   // public static  Socket sslsocket;
    private SSLSocketFactory sslsocketfactory;
    public static Socket sslsocket;
    public static Thread th;
    public static String Error;


    // Functions


     
    XMPP(String XUser,String XPass,String XServer)
    {
        XMPPUser=XUser;
        XMPPPass=XPass;
        XMPPSer=XServer;
        XMPPDom=XServer;
        Error="No_Error";
    }
    // To initialize general objects
    

    void initialize(Socket ssl) throws IOException,javax.xml.stream.XMLStreamException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException
    {
        
            factory = XMLOutputFactory.newInstance();
            writer = factory.createXMLStreamWriter(ssl.getOutputStream()) ;
           System.out.println("initialized");
    }

    
    @Override
    public void run () {
        // TODO code application logic here
        
        try
        {    
         
        
         // sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
         // sslsocket = (SSLSocket) sslsocketfactory.createSocket("localhost",5222);
            sslsocket = new Socket("127.0.0.1",5222);

           if(sslsocket.isConnected())
           {
               System.out.println("Connected");
               System.out.println(XMPPDom);
          initialize(sslsocket);
       
           writer.writeStartDocument("1.0");
           writer.setPrefix("stream","http://etherx.jabber.org/streams");
           writer.writeStartElement("http://etherx.jabber.org/streams","stream");
           writer.writeAttribute("xmlns","jabber:client");
           writer.writeAttribute("to", XMPPDom);
           writer.writeNamespace("stream", "http://etherx.jabber.org/streams");
           writer.writeAttribute("version", "1.0");
           writer.writeCharacters("");
           writer.flush();

        
           Thread.sleep(2000);

           initialize(sslsocket);
           System.out.println("Socket Upgraded");
          
             Thread.sleep(1000);
           }
           else{
               
               System.out.println("Failed to connect");
           }
   

        System.out.println("Handshaked");
        XMLParser XmlParse = new XMLParser();
          th=new Thread(XmlParse);
          th.start();
          Thread.sleep(2000);
          
          
         System.out.println("1");   
         
          writer.writeStartElement("auth");
          writer.writeAttribute("xmlns","urn:ietf:params:xml:ns:xmpp-sasl");
          writer.writeAttribute("mechanism","PLAIN");
          writer.writeAttribute("client-uses-full-bind-result","true");
          writer.writeCharacters(new BASE64Encoder().encode(((char)0+XMPPUser+(char)0+XMPPPass).getBytes()));
          writer.writeEndElement();
          writer.flush();
         
           writer.writeStartElement("iq");
           writer.writeAttribute("type","set");
           writer.writeAttribute("id","purple171edb0c");
           writer.writeStartElement("bind");
           writer.writeAttribute("xmlns","urn:ietf:params:xml:ns:xmpp-bind");
           writer.writeStartElement("resource");
           writer.writeCharacters("purple");
           writer.writeEndElement();
           writer.writeEndElement();
           writer.writeEndElement();
           writer.flush();

          // th.sleep(2000); 

           
           
          // th.sleep(2000); 
           
           writer.writeStartElement("presence");
           writer.writeStartElement("priority");
           writer.writeCharacters("1");
           writer.writeEndElement();
           writer.writeStartElement("c");
           writer.writeAttribute("xmlns","http://jabber.org/protocol/caps");
           writer.writeAttribute("node","http://pidgin.im/");
           writer.writeAttribute("hash","sha-1");
           writer.writeAttribute("ver","71LAP/wlWGfun7j+Q4FCSSuAhQw=");
           writer.writeEndElement();
           writer.writeStartElement("x");
           writer.writeAttribute("xmlns","vcard-temp:x:update'");
           writer.writeEndElement();
           
           writer.writeEndElement();
           writer.flush();
           
                      
           writer.writeStartElement("iq");
           writer.writeAttribute("type","get");
           writer.writeAttribute("id", "purple171edb14");
           writer.writeAttribute("to", "1j6oopmb5c1p30kxodibyj9alr@public.talk.google.com/MessagingA9a0aab6e");
           writer.writeStartElement("query");
           writer.writeAttribute("xmlns","http://jabber.org/protocol/disco#info");
           writer.writeAttribute("node","http://www.android.com/gtalk/client/caps#1.1");
           writer.writeEndElement();
           writer.writeEndElement();
           writer.flush();
           
                      writer.writeStartElement("iq");
           writer.writeAttribute("type","get");
           writer.writeAttribute("id", "purple171edb15");
           writer.writeStartElement("ping");
           writer.writeAttribute("xmlns","urn:xmpp:ping'");
           writer.writeEndElement();
           writer.writeEndElement();
           writer.flush();
           
           
           writer.writeStartElement("presence");
           writer.writeStartElement("show");
           writer.writeCharacters("chat");
           writer.writeEndElement();
           writer.writeEndElement();
           writer.flush();
           

           
          writer.writeStartElement("iq");
          writer.writeAttribute("type","get");
          writer.writeEmptyElement("query");
          writer.writeAttribute("xmlns", "jabber:iq:roster");
          writer.writeEndElement();
          writer.writeCharacters("\n");
          writer.flush();
        }
        
        catch(Exception e){
            System.out.println(e);
            Error=e.toString();
        
        };
        
    }
    public static void main(String[] args) throws XMLStreamException, IOException {
        
        new XMPPClient().setVisible(true);
    }
}
/*